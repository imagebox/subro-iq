<?php

/**
 * BoxPress functions and definitions
 *
 * @package BoxPress
 */

if ( ! function_exists( 'boxpress_setup' )) :
function boxpress_setup() {

  add_theme_support( 'automatic-feed-links' );
  add_theme_support( 'title-tag' );
  add_theme_support( 'post-thumbnails', array( 'post', 'page' ));
  add_theme_support( 'html5', array(
    'search-form',
    'comment-form',
    'comment-list',
    'gallery',
    'caption',
  ));

  add_image_size( 'home_hero', 1800, 1000, true );
  add_image_size( 'home_index_thumb', 860, 350, true );

  add_image_size( 'card_thumb', 414, 258, true );

  add_image_size( 'content_hero', 1800, 917, false );
  add_image_size( 'content_full_width', 1800, 543, true );
  add_image_size( 'content_half_width', 900, 586, true );

  add_image_size( 'intro_img', 600, 600, true );

}
endif;
add_action( 'after_setup_theme', 'boxpress_setup' );



/**
 * Enqueue scripts and styles.
 */

function boxpress_scripts() {

  /**
   * Styles
   */

  $style_font_url     = 'https://fonts.googleapis.com/css?family=Montserrat:300,400,500,600|Open+Sans:400,400i,600,700';
  $style_screen_path  = get_stylesheet_directory_uri() . '/assets/css/style.min.css';
  $style_screen_ver   = filemtime( get_stylesheet_directory() . '/assets/css/style.min.css' );
  $style_print_path   = get_stylesheet_directory_uri() . '/assets/css/print.css';
  $style_print_ver    = filemtime( get_stylesheet_directory() . '/assets/css/print.css' );

  wp_enqueue_style( 'google-fonts', $style_font_url, array(), false, 'screen' );
  wp_enqueue_style( 'screen', $style_screen_path, array('google-fonts'), $style_screen_ver, 'screen' );
  wp_enqueue_style( 'print', $style_print_path, array( 'screen' ), $style_print_ver, 'print' );


  /**
   * Scripts
   */

  $script_modernizr_path  = get_template_directory_uri() . '/assets/js/dev/modernizr.js';
  $script_modernizr_ver   = filemtime( get_template_directory() . '/assets/js/dev/modernizr.js' );
  $script_site_path = get_template_directory_uri() . '/assets/js/build/site.min.js';
  $script_site_ver  = filemtime( get_template_directory() . '/assets/js/build/site.min.js' );

  wp_enqueue_script( 'modernizr', $script_modernizr_path, array(), $script_modernizr_ver, false );
  wp_enqueue_script( 'site', $script_site_path, array( 'jquery' ), $script_site_ver, true );

  // Single Posts
  if ( is_singular() && comments_open() &&
       get_option( 'thread_comments' )) {
    wp_enqueue_script( 'comment-reply' );
  }
}
add_action( 'wp_enqueue_scripts', 'boxpress_scripts' );



/**
 * Query for child pages of current page
 *
 * @param  array  $options Accepts post type and depth.
 * @return string          Returns WP list of pages in html.
 */
function query_for_child_page_list( $options = array() ) {
  $default_options = array(
    'post_type' => 'page',
    'depth'     => 4,
  );
  $config = array_merge( $default_options, $options );

  global $wp_query;
  $post = $wp_query->post;

  if ( $post ) {
    $parent_ID = $post->ID;

    if ( $post->post_parent !== 0 ) {
      $ancestors  = get_post_ancestors( $post );
      $parent_ID  = end( $ancestors );
    }

    $list_pages_args = array(
      'post_type' => $config['post_type'],
      'title_li'  => '',
      'child_of'  => $parent_ID,
      'depth'     => $config['depth'],
      'echo'      => false,
    );

    // Get list of pages
    return wp_list_pages( $list_pages_args );
  }
}


/**
 * Menu setup
 */
require get_template_directory() . '/inc/site-navigation.php';

/**
 * Accessibility
 */
require get_template_directory() . '/inc/walkers/class-aria-walker-nav-menu.php';

/**
 * Cleanup the header
 */
require get_template_directory() . '/inc/cleanup.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Admin Related Functions
 */
require get_template_directory() . '/inc/admin.php';
require get_template_directory() . '/inc/editor.php';

/**
 * ACF
 */
// Options Pages
require get_template_directory() . '/inc/acf-options.php';
// Search Queries
require get_template_directory() . '/inc/acf-search.php';

/**
 * Plugin Settings
 */
require get_template_directory() . '/inc/plugin-settings/gravity-forms.php';
require get_template_directory() . '/inc/plugin-settings/tribe-events.php';


/**
 * CAREERS CPT
 */

require get_template_directory() . '/inc/cpt/novum-blog.php';
