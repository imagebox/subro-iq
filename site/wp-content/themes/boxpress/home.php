<?php
/**
 * The template for displaying the Blog page.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package BoxPress
 */

get_header(); ?>

  <?php require_once('template-parts/banners/banner--blog.php');?>

  <section class="blog-page">
    <div class="wrap">

      <div class="l-sidebar">
        <div class="l-main">

          <?php if ( have_posts() ) : ?>

            <header class="page-header">
              <?php
                the_archive_title( '<h1 class="page-title">', '</h1>' );
                the_archive_description( '<div class="taxonomy-description">', '</div>' );
              ?>
            </header><!-- .page-header -->

            <?php while ( have_posts() ) : the_post(); ?>

              <?php get_template_part( 'template-parts/content', get_post_format() ); ?>

            <?php endwhile; ?>

            <?php the_posts_navigation(); ?>

          <?php else : ?>

            <?php get_template_part( 'template-parts/content', 'none' ); ?>

          <?php endif; ?>

        </div>
        <div class="l-aside">

          <?php get_sidebar(); ?>

        </div>
      </div>
    </div>
  </section>

<?php get_footer(); ?>
