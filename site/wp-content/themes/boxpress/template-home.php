<?php
/**
 * Template Name: Homepage
 */

 $heading = get_sub_field('heading');
 $description = get_sub_field('carousel_description');
 $background = get_sub_field('background');
 $hero_subhead           = get_field('hero_subhead');
 $hero_background        = get_field('background');
 $hero_background_image  = get_field('hero_background_image');
 $top_bar_number = get_field('top_bar_number');
 $hero_link      = get_field('hero_link');
 $hero_link_text = get_field('hero_link_text');

get_header(); ?>

  <article class="homepage">

  <?php get_template_part('template-parts/homepage-hero'); ?>

    <section class="home-section">
      <section class="homepage-mobile-hero-content para-hide section">
        <div class="wrap">
          <div class="hero-box">
              <div class="hero-content">
                <h1><?php the_field( 'hero_heading' ); ?></h1>

                <?php if ( ! empty( $hero_subhead )) : ?>
                  <h6><?php the_field( 'hero_subhead' ); ?></h6>
                <?php endif; ?>

                <a class="video-button button button--icon-1" href="<?php the_field('v_link');?>">
                  Play Video
                </a>
              </div>
          </div>
        </div>
      </section>
      <!-- mobile hero  -->

       <section class="callout-block">
         <div class="wrap">
           <?php
            $callout_image_block = get_field('callout_image_block_1');
            if( !empty($callout_image_block)):
              $url = $callout_image_block['url'];
              $size = 'intro_img';
              $para_callout_image = $callout_image_block['sizes'][ $size ];
            ?>
            <div class="callout-image-block">
                <div class="callout-image" style="background-image:url(<?php echo $para_callout_image; ?>); background-size: cover; background-repeat: no-repeat;">
                <div class="overlay"></div>
               <div class="callout-heading">
                    <h2><?php the_field('callout_heading_block_1') ?></h2>
               </div>
                </div>
            </div>

           <?php endif; ?>
           <!-- intro block  -->


            <div class="callout-content-block">
                <div class="callout-content pad--med">
                <?php $callout_content_1 = get_field('callout_content_1'); ?>
                <?php echo $callout_content_1; ?>

                      <?php
                       $callout_link_block_1 = get_field('callout_link_block_1');
                       if( $callout_link_block_1): ?>
                        <a class="button button--icon-1" href="<?php echo $callout_link_block_1['url']; ?>" target="<?php echo $callout_link_block_1['target']; ?>"><?php echo $callout_link_block_1['title']; ?></a>
                    </div>
              </div>
            </div>

           <?php endif; ?>
           </div>
           <!-- carousel-block  -->
         </div>
       </section>
 <!-- callout block  -->


 <?php if( have_rows('overview_link_block') ): ?>

 	<section class="overview-link-block section">
    <div class="wrap">
      <?php
      	$overview_heading = get_field('overview_heading');
        $overview_heading_button = get_field('overview_heading_button');
      ?>

      <div class="overview-heading">
        <h2><?php echo $overview_heading; ?></h2>

        <?php
        if( $overview_heading_button ): ?>
          <h6>
            <a class="button button--icon-1" href="<?php echo $overview_heading_button['url']; ?>" target="<?php echo $overview_heading_button['target']; ?>">
              <?php echo $overview_heading_button['title']; ?>
            </a>
          </h6>
      </div>


      <?php endif; ?>

      <div class="l-grid l-grid--four-col">
        <?php while( have_rows('overview_link_block') ): the_row();

       		// vars
          $overview_sub_heading = get_sub_field('overview_sub_heading');
       		$overview_link = get_sub_field('overview_link');
          $overview_icon = get_sub_field('overview_icon');
          $overview_copy = get_sub_field('overview_copy');
       		?>
         		<div class="l-grid-item">

                <?php if( $overview_link ): ?>
                  <a href="<?php echo $overview_link; ?>">
                <?php endif; ?>

                 <div class="overview-grid-button">
                   <div class="overview-card-header">
                     <h4><?php echo $overview_sub_heading; ?></h4>
                     <img src="<?php echo $overview_icon['url']; ?>" alt="<?php echo $overview_icon['alt'] ?>" />
                   </div>
                   <div class="overview-card-body">
                     <p><?php echo $overview_copy; ?></p>
                   </div>
                  </div>

                <?php if( $overview_link ): ?>
                  </a>
                <?php endif; ?>
         		</div>
       	<?php endwhile; ?>
      </div>
    </div>
  </section>

 <?php endif; ?>
<!-- grid block -->


<section class="callout-block">
  <div class="wrap">
    <?php
     $callout_image_block_2 = get_field('callout_image_block_2');
     if( !empty($callout_image_block_2)):
       $url = $callout_image_block_2['url'];
       $size = 'intro_img';
       $my_callout_image_2 = $callout_image_block_2['sizes'][ $size ];
     ?>
     <div class="callout-image-block callout--right">
         <div class="callout-image text-align--right" style="background-image:url(<?php echo $my_callout_image_2; ?>); background-size: cover; background-repeat: no-repeat;">
         <div class="overlay-2"></div>
        <div class="callout-heading switch-margin--right">
             <h2><?php the_field('callout_heading_block_2') ?></h2>
        </div>
         </div>
     </div>

    <?php endif; ?>
    <!-- intro block  -->



     <div class="callout-content-block callout--left">
             <div class="callout-content pad--med">
               <?php $callout_content_2 = get_field('callout_content_2'); ?>
               <?php echo $callout_content_2; ?>

               <?php
                $callout_link_block_2 = get_field('callout_link_block_2');
                if( $callout_link_block_2): ?>
                 <a class="button button--icon-1" href="<?php echo $callout_link_block_2['url']; ?>" target="<?php echo $callout_link_block_2['target']; ?>"><?php echo $callout_link_block_2['title']; ?></a>
                <?php endif; ?>
             </div>
       </div>
     </div>
    </div>
    <!-- carousel-block  -->
  </div>
</section>
<!-- callout block 2  -->



  <?php if( have_rows('logo_block') ): ?>
  	<section class="logo-block">
      <div class="wrap">
         <h4><?php the_field('logo_heading_block') ?></h4>
        <div class="logo-item-block">
          <?php while( have_rows('logo_block') ): the_row();
        		// vars
        		$associates_logos_block = get_sub_field('associates_logos');
            $link = get_sub_field('link');

        		?>
          	<a href="<?php echo $link['url']; ?>" target="<?php echo $link['target']; ?>">
        			<img src="<?php echo $associates_logos_block ['url']; ?>" alt="<?php echo $associates_logos_block ['alt'] ?>" />
            </a>
          <?php endwhile; ?>
        </div>
      </div>
  	</section>

  <?php endif; ?>



  </article>

<?php get_footer(); ?>
