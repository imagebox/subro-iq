<?php
/**
 * Displays the Hero layout
 *
 * @package BoxPress
 */
$hero_subhead           = get_field('hero_heading');
$hero_background        = get_field('background');
$hero_background_image  = get_field('hero_background_image');

$hero_link      = get_field('hero_link');

?>


<section class="homepage-hero">
  <div class="hero-overlay"></div>
  <video class="bg-video vs-source" playsinline autoplay muted loop>
  <source src="<?php bloginfo('template_directory');?>/assets/video/hero-video-paragon.mp4" type="video/mp4">
  </video>

  <div class="wrap">
    <div class="hero-box">
        <div class="hero-content">


          <?php if ( ! empty( $hero_subhead )) : ?>
            <h1><?php the_field( 'hero_heading' ); ?></h1>
          <?php endif; ?>

            <!-- <a class="video-button button button--icon-1" href="<?php the_field('v_link');?>">
              Play Video
            </a> -->

        </div>
    </div>
  </div>
</section>
