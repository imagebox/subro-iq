<?php
/**
 * The template used for displaying page content in page.php
 *
 * @package BoxPress
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

  <header class="page-header">

  </header>

  <div class="page-content">
    <?php the_content(); ?>
  </div>

</article>
