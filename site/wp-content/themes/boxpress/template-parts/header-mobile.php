
<div class="site-header--mobile">
  <div class="mobile-header-left">
    <div class="site-branding">
      <a href="<?php echo esc_url( home_url( '/' )); ?>" rel="home">
        <img src="<?php echo get_bloginfo( 'template_directory' ); ?>/assets/img/branding/site-logo.png" />
      </a>
    </div>
  </div>



  <div class="mobile-header-right">
    <a href="#" class="menu-button toggle-nav">
      <span class="screen-reader-text"><?php _e('Menu', 'boxpress'); ?></span>
    <img src="<?php echo get_bloginfo( 'template_directory' ); ?>/assets/img/menu-icon.png" />
    </a>
  </div>
</div>

<div class="mobile-nav-tray">
  <nav class="navigation--mobile">
    <div class="mobile-nav-header">
      <div class="site-branding">
        <a href="<?php echo esc_url( home_url( '/' )); ?>" rel="home">
          <img src="<?php echo get_bloginfo( 'template_directory' ); ?>/assets/img/branding/site-logo.png" />
        </a>
      </div>
      <a class="menu-button toggle-nav" href="#">
        <img src="<?php echo get_bloginfo( 'template_directory' ); ?>/assets/img/close-icon.png" />
      </a>
    </div>

    <?php if ( has_nav_menu( 'primary' ) ) : ?>
      <ul class="mobile-nav mobile-nav--main">
        <?php
          wp_nav_menu( array(
            'theme_location'  => 'primary',
            'items_wrap'      => '%3$s',
            'container'       => false,
          ));
        ?>
      </ul>
    <?php endif; ?>

    <?php if ( has_nav_menu( 'secondary' ) ) : ?>
      <ul class="mobile-nav mobile-nav--utility">
        <?php
          wp_nav_menu( array(
            'theme_location'  => 'secondary',
            'items_wrap'      => '%3$s',
            'container'       => false,
          ));
        ?>
      </ul>
    <?php endif; ?>

    <div class="mobile-nav--search">
    </div>
  </nav>
</div>
