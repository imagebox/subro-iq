<?php
/**
 * Displays the Slideshow layout
 *
 * @package BoxPress
 */
?>
<?php if ( have_rows( 'home-carousel' )) : ?>

  <div class="home-carousel">
    <div class="js-owl-carousel owl-carousel owl-theme">

      <?php while ( have_rows( 'home-carousel' )) : the_row();
          $slide_heading  = get_sub_field( 'slide_heading' );
          $slide_subhead  = get_sub_field( 'slide_subhead' );
          $slide_bkg      = get_sub_field( 'slide_bkg' );
          $slide_hidden   = get_sub_field( 'is_slide_hidden' );
        ?>
        <?php if ( ! $slide_hidden ) : ?>

          <div class="carousel-slide">

            <?php if ( $slide_bkg ) :
                $image_size     = 'home_slideshow';
                $slide_bkg_url  = $slide_bkg['sizes'][ $image_size ];
                $slide_bkg_w    = $slide_bkg['sizes'][ $image_size . '-width' ];
                $slide_bkg_h    = $slide_bkg['sizes'][ $image_size . '-height' ];
              ?>
              <img class="slide-bkg"
                src="<?php echo $slide_bkg_url; ?>"
                width="<?php echo $slide_bkg_w; ?>"
                height="<?php echo $slide_bkg_h; ?>"
                alt="">
            <?php endif; ?>

            <div class="wrap slide-wrap">
              <div class="slide-content">

                <h2 class="h1 slide-title"><?php echo $slide_heading; ?></h2>
                <p class="slide-sub-title"><?php echo $slide_subhead; ?></p>

              </div>
            </div>
          </div>

        <?php endif; ?>
      <?php endwhile; ?>

    </div>
  </div>

<?php endif; ?>
