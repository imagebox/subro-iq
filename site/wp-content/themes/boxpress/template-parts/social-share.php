
<div class="sharing">
  <h5><?php _e( 'Share on:', 'boxpress' ); ?></h5>
  <a class="js-social-share"
     target="_blank"
     href="https://www.facebook.com/sharer/sharer.php?u=<?php echo urlencode('' . the_permalink() . ''); ?>">
    <svg class="social-facebook-svg" width="31" height="31">
      <use xlink:href="#social-facebook"></use>
    </svg>
  </a>
  <a class="js-social-share"
     target="_blank"
     href="https://twitter.com/intent/tweet/?text=<?php the_title(); ?>&url=<?php echo urlencode('' . the_permalink() . ''); ?>">
    <svg class="social-twitter-svg" width="31" height="31">
      <use xlink:href="#social-twitter"></use>
    </svg>
  </a>
  <a class="js-social-share"
     target="_blank"
     href="https://plus.google.com/share?url=<?php echo urlencode('' . the_permalink() . ''); ?>">
    <svg class="social-google_plus-svg" width="31" height="31">
      <use xlink:href="#social-google_plus"></use>
    </svg>
  </a>
  <a class="js-social-share"
     target="_blank"
     href="https://www.linkedin.com/shareArticle?mini=true&url=<?php echo urlencode('' . the_permalink() . ''); ?>&title=<?php echo urlencode( '' . get_the_title() . '' ); ?>&source=<?php echo urlencode('' . the_permalink() . ''); ?>&summary=<?php echo urlencode( '' . strip_tags( get_the_excerpt() ) . '' ); ?>">
    <svg class="social-linkedin-svg" width="31" height="31">
      <use xlink:href="#social-linkedin"></use>
    </svg>
  </a>
</div>
