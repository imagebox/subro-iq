<?php
/**
 * Displays the blog banner tamplate part
 */

$banner_title = 'Blog';
$banner_image_url = '';
$default_banner   = get_field( 'default_banner_image', 'option' );
$blog_banner      = get_field( 'blog_banner_image', 'option' );

if ( $blog_banner ) {
  $banner_image_url = $blog_banner['url'];
} elseif ( $default_banner ) {
  $banner_image_url = $default_banner['url'];
}

?>
<header class="banner">
  <div class="wrap">
    <div class="banner-title">
      <span class="h1"> 
        <?php echo $banner_title; ?>
      </span>
    </div>
    <?php if ( ! empty( $banner_image_url ) ) : ?>
      <div class="banner-image">
        <img src="<?php echo $banner_image_url; ?>" alt="">
      </div>
    <?php endif; ?>
  </div>
</header>
