<?php
/**
 * Displays the 404 page banner
 *
 * @package BoxPress
 */

$banner_title     = 'Search';
$banner_image_url = '';
$default_banner   = get_field( 'default_banner_image', 'option' );

if ( $default_banner ) {
  $banner_image_url = $default_banner['url'];
}

?>
<header class="banner">
  <div class="wrap">
    <div class="banner-title">
      <span class="h1"> 
        <?php echo $banner_title; ?>
      </span>
    </div>
    <div class="banner-image">
      <img src="<?php echo $banner_image_url; ?>" alt="">
    </div>
  </div>
</header>
