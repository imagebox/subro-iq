<?php
/**
 * The template for displaying the Blog page.
 *
 * Template Name: Blog Two
 *
 * @package BoxPress
 */

get_header(); ?>

  <?php require_once('template-parts/banners/banner--blog.php');?>

  <section class="blog-page">
    <div class="wrap">

      <div class="l-sidebar">
        <div class="l-main">

          <?php
           $regular_post_query = array(
             'post_type' => 'blog',
             'posts_per_page' => 9,
           );
           $regular_post_loop = new WP_Query( $regular_post_query );
           ?>
           <div class="l-grid-wrap">
             <div id="ajax-load-more-container" class="l-grid l-grid--three-col l-grid--gutter-small">

               <?php  while ( $regular_post_loop->have_posts() ) : $regular_post_loop->the_post();?>

                 <div class="l-grid-item">
                         <?php get_template_part( 'template-parts/content', get_post_format() ); ?>
                 </div>

               <?php
                 endwhile;
            ?>
        </div>

        <div class="l-aside">

          <?php get_sidebar('blog'); ?>

        </div>
      </div>
    </div>
  </section>
<?php get_footer(); ?>
