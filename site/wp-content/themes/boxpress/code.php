<?php
/**
 * Template Name: Careers Blog
 */
get_header(); ?>

<?php include_once('template-parts/mobile-cat-nav-career.php'); ?>

<?php

/**
 * Displays an article card
 */




 ?>

    <?php
    wp_reset_query();
    //if ( !is_paged() ) {


      $post_feature = get_the_post_thumbnail( get_the_ID(), 'home_slideshow', '' );
      $card_terms = get_the_terms( get_the_ID(), 'careers_categories' );
      $card_term_name = '';
      $card_term_slug = '';

      if ( $card_terms && ! is_wp_error( $card_terms )) {
        foreach ( $card_terms as $term ) {
          $card_term_name = $term->name;
          $card_term_slug = $term->slug;
        }
      }

      $sticky = get_option( 'sticky_posts' );
      $args = array(
            'post_type' => 'careers',
            'posts_per_page' => -1,
            'post__in'  => $sticky,
            'ignore_sticky_posts' => 1
      );
      $query = new WP_Query( $args );
      if ( isset( $sticky[0] ) ) {
          ?>


          <?php  while ( $query->have_posts() ) : $query->the_post();?>

        <div class="featured-post feature-desktop-hide-show">


          <?php
          $feature_image_bg      = get_field( 'feature_image_bg' );
          $feature_image_bg_size = 'feature_banner_news';
           ?>

           <?php echo $post_feature; ?>

            <img class="feature-post-bgimage"
            src="<?php echo esc_url( $feature_image_bg['sizes'][ $feature_image_bg_size ] ); ?>"
            width="<?php echo esc_attr( $feature_image_bg['sizes'][ $feature_image_bg_size . '-width'] ); ?>"
            height="<?php echo esc_attr( $feature_image_bg['sizes'][ $feature_image_bg_size . '-height'] ); ?>"
            alt="<?php echo esc_attr( $feature_image_bg['alt'] ); ?>">


            <div class="card-article">
                <div class="card-article-body">
                  <div class="feature-wrap">
                  <?php if ( ! empty( $card_term_name ) && ! empty( $card_term_slug )) : ?>
                      <div class="feature-icon">
                        <svg class="icon-<?php echo $term->slug; ?>" width="30" height="30">
                          <use xlink:href="#icon-<?php echo $term->slug; ?>">
                        </svg>
                      </div>
                      <h5 class="card-article-cat"><?php echo $card_term_name; ?></h5>
                    <?php endif; ?>
                  <a href="<?php the_permalink(); ?>">
                    <h3 class="card-article-title"><?php the_title(); ?></h3>
                    <div class="readmore-block">
                      <span>Read More</span>
                      <svg class="" width="20" height="20">
                        <use xlink:href="#feature-more"></use>
                      </svg>
                    </div>
                  </a>
                  </div>
              </div>
          </div>
        </div>

        <div class="featured-post-mobile feature-mobile-hide-show">
            <div class="card-article">
                <div class="card-article-body">
                  <div class="feature-wrap">
                  <?php if ( ! empty( $card_term_name ) && ! empty( $card_term_slug )) : ?>
                    <div class="feature-icon">
                      <svg class="icon-<?php echo $term->slug; ?>" width="30" height="30">
                        <use xlink:href="#icon-<?php echo $term->slug; ?>">
                      </svg>
                    </div>
                      <h5 class="card-article-cat"><?php echo $card_term_name; ?></h5>
                    <?php endif; ?>
                  <a href="<?php the_permalink(); ?>">
                    <h3 class="card-article-title"><?php the_title(); ?></h3>
                    <div class="readmore-block">
                      <span>Read More</span>
                      <svg class="" width="20" height="20">
                        <use xlink:href="#feature-more"></use>
                      </svg>
                    </div>
                  </a>
                  </div>
              </div>
          </div>
        </div>

    <?php endwhile; ?>

    <?php } wp_reset_query();
    //}
  ?>

  <section class="news-page">
    <div class="wrap">

      <div class="l-sidebar">
        <div class="l-main">

          <header class="vh page-header">
               <h1 class="page-title"><?php _e('Career Blog', 'boxpress'); ?></h1>
           </header>

            <?php
            $regular_post_query = array(
              'post_type' => 'careers',
              'posts_per_page' => 9,
              'post__not_in' => get_option( 'sticky_posts' ),
              'paged' => $paged,
            );
            $regular_post_loop = new WP_Query( $regular_post_query );
            ?>
            <div class="l-grid-wrap">
              <div id="ajax-load-more-container" class="l-grid l-grid--three-col l-grid--gutter-small">


                <?php  while ( $regular_post_loop->have_posts() ) : $regular_post_loop->the_post();?>

                  <div class="l-grid-item">
                    <?php get_template_part( 'template-parts/cards/card-careers-article' ); ?>
                  </div>

                <?php
                  endwhile;
                ?>

            </div>
          </div>

          <?php imagebox_numeric_posts_nav(); ?>

          <div class="popular-post-mobile">

            <?php
              add_filter( 'get_search_form', 'extra_search_form_careers' );
              get_search_form();
              remove_filter( 'get_search_form', 'extra_search_form_careers' );
            ?>


            <?php
                /**
                 * Blog Archive Links
                 */

                $blog_archives = wp_get_archives( array(
                  'type'            => 'monthly',
                  'format'          => 'option',
                  'echo'            => false,
                ));
              ?>

              <?php if ( $blog_archives ) : ?>

                <div class="sidebar-widget">
                  <h4 class="widget-title archive-title"><?php _e('Archives', 'boxpress'); ?></h4>
                  <div class="archives-widget news-select">
                    <form action="<?php echo get_permalink( get_option( 'page_for_posts' )); ?>">
                      <label class="vh" for="archive_dropdown"><?php _e('Select Year', 'boxpress'); ?></label>
                      <select id="archive_dropdown" class="ui-select" name="archive_dropdown" onchange="document.location.href=this.options[this.selectedIndex].value;">
                        <option value=""><?php echo _e( 'Select Month' ); ?></option>
                        <?php echo $blog_archives; ?>
                      </select>
                    </form>
                  </div>
                </div>

              <?php endif; ?>

          </div>

        </div>
        <div class="l-aside">

          <?php get_sidebar('careers'); ?>

        </div>
      </div>
    </div>
  </section>

<?php get_footer(); ?>

<!-- blog template code  -->

<?php
/**
 * The template for displaying archive pages.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package BoxPress
 */

get_header(); ?>

<?php require_once('template-parts/banners/banner--archive.php'); ?>

<?php include_once('template-parts/mobile-cat-nav-career.php'); ?>

<?php

/**
 * Displays an article card
 */

$post_feature = get_the_post_thumbnail( get_the_ID(), 'home_slideshow', '' );
$card_terms = get_the_terms( get_the_ID(), 'careers_categories' );

$card_term_name = '';
$card_term_slug = '';

if ( $card_terms && ! is_wp_error( $card_terms )) {
  foreach ( $card_terms as $term ) {
    $card_term_name = $term->name;
    $card_term_slug = $term->slug;
  }
}


 ?>



  <section class="news-page">
    <div class="wrap">

      <div class="l-sidebar">
        <div class="l-main">

          <header class="vh page-header">
               <h1 class="page-title"><?php _e('News', 'boxpress'); ?></h1>
           </header>

          <?php
        /**
         * Featured Posts
         */

        $query_featured_posts_args = array(
          'post_type' => 'careers',
          'posts_per_page' => 3,
          'tax_query' => array(
            array(
              'taxonomy'  => 'featured_post',
              'field'     => 'slug',
              'terms'     => 'blog-page',
            ),
          ),
        );
        $query_featured_posts = new WP_Query( $query_featured_posts_args );
      ?>
      <?php if ( $query_featured_posts->have_posts() ) : ?>

        <div class="blog-featured-posts">
          <div class="js-hero-carousel owl-carousel owl-theme">

            <?php while ( $query_featured_posts->have_posts() ) : $query_featured_posts->the_post();
                $card_terms = get_the_terms( get_the_ID(), 'category' );
              ?>

              <div class="featured-post-slide">
                <?php if ( has_post_thumbnail() ) : ?>
                  <?php the_post_thumbnail( 'article_thumb' ); ?>
                <?php endif; ?>

                <div class="featured-post-content">
                  <?php if ( $card_terms && ! is_wp_error( $card_terms )) : ?>
                    <h4>
                      <?php foreach ( $card_terms as $term ) : ?>
                        <span><?php echo $term->name; ?></span>
                      <?php endforeach; ?>
                    </h4>
                  <?php endif; ?>
                  <h3><?php the_title(); ?></h3>

                </div>
              </div>

            <?php endwhile; ?>

          </div>
        </div>

        <?php wp_reset_postdata(); ?>
      <?php endif; ?>


      <?php if ( have_posts() ) : ?>

        <div class="l-grid-wrap">
          <div id="ajax-load-more-container" class="l-grid l-grid--three-col l-grid--gutter-small">

            <?php while ( have_posts() ) : the_post(); ?>

              <div class="l-grid-item">
                <?php get_template_part( 'template-parts/cards/card-career-article' ); ?>
              </div>

            <?php endwhile; ?>

          </div>
        </div>
        <?php imagebox_numeric_posts_nav(); ?>

        <?php get_template_part( 'template-parts/content/content', 'none' ); ?>

      <?php endif; ?>

      <div class="popular-post-mobile">
        <?php
          add_filter( 'get_search_form', 'extra_search_form' );
          get_search_form();
          remove_filter( 'get_search_form', 'extra_search_form' );
        ?>


        <?php
            /**
             * Blog Archive Links
             */

            $blog_archives = wp_get_archives( array(
              'type'            => 'monthly',
              'format'          => 'option',
              'echo'            => false,
            ));
          ?>

          <?php if ( $blog_archives ) : ?>

            <div class="sidebar-widget">
              <h4 class="widget-title archive-title"><?php _e('Archives', 'boxpress'); ?></h4>
              <div class="archives-widget news-select">
                <form action="<?php echo get_permalink( get_option( 'page_for_posts' )); ?>">
                  <label class="vh" for="archive_dropdown"><?php _e('Select Year', 'boxpress'); ?></label>
                  <select id="archive_dropdown" class="ui-select" name="archive_dropdown" onchange="document.location.href=this.options[this.selectedIndex].value;">
                    <option value=""><?php echo _e( 'Select Month' ); ?></option>
                    <?php echo $blog_archives; ?>
                  </select>
                </form>
              </div>
            </div>

          <?php endif; ?>


      </div>

    </div>
        <div class="l-aside">

          <?php get_sidebar('careers'); ?>

        </div>
      </div>
    </div>
  </section>



<?php get_footer(); ?>

 <!-- taxonomy-careers_categories  -->


 <?php
/**
 * Displays the sidebar
 *
 * @package BoxPress
 */
?>
<aside class="sidebar" role="complementary">


    <h1 class="blog-sidebar-title"><?php _e('Career Blog', 'boxpress'); ?></h1>

    <?php
      /**
       * Blog Category Links
       */
      $blog_current_term = '';

      $blog_cats = get_terms( array(
        'taxonomy'    => 'careers_categories',
        // 'hide_empty'  => false,
        'exclude'     => '1',
      ));

      $blog_current_term = get_queried_object()->term_id;
    ?>

    <?php if ( $blog_cats && ! is_wp_error( $blog_cats )) : ?>

      <div class="sidebar-widget">
        <h4 class="widget-title"><?php _e('Categories', 'boxpress'); ?></h4>
        <nav class="categories-widget">
          <ul>
            <li class="<?php
                if ( is_home() ) {
                  echo 'is-current-cat';
                }
              ?>">
              <a href="/careers-blog/">
                <svg class="side-bar-nav-cat" width="30" height="30">
                  <use xlink:href="#icon-category">
                </svg>
                <?php _e('All Categories', 'boxpress'); ?>
              </a>
            </li>

            <?php foreach ( $blog_cats as $term ) : ?>

              <li class="<?php
                  if ( $blog_current_term === $term->term_id ) {
                    echo 'is-current-cat';
                  }
                ?>">
                <div class="side-bar-nav-cat">
                  <svg class="icon-<?php echo $term->slug; ?>" width="30" height="30">
                    <use xlink:href="#icon-<?php echo $term->slug; ?>">
                  </svg>
                </div>
                <a href="<?php echo esc_url( get_term_link( $term )); ?>">
                  <?php echo $term->name; ?>
                </a>
              </li>

            <?php endforeach; ?>

          </ul>
        </nav>
      </div>

    <?php endif; ?>


    <?php
      add_filter( 'get_search_form', 'extra_search_form_careers' );
      get_search_form();
      remove_filter( 'get_search_form', 'extra_search_form_careers' );
    ?>


    <?php
  /**
   * Blog Archive Links
   */

   $careers_archive = array(
       'post_type'    => 'careers',
       'type'         => 'monthly',
       'format'          => 'option',
       'echo'         => false
   );
?>

<?php if ( $careers_archive ) : ?>

  <div class="sidebar-widget">
    <h4 class="widget-title"><?php _e('Archives', 'boxpress'); ?></h4>
    <div class="archives-widget news-select">
      <form action="<?php echo get_permalink( get_option( 'page_for_posts' )); ?>">
        <label class="vh" for="archive_dropdown"><?php _e('Select Year', 'boxpress'); ?></label>
        <select id="archive_dropdown" class="ui-select" name="archive_dropdown" onchange="document.location.href=this.options[this.selectedIndex].value;">
          <option value=""><?php echo _e( 'Select Month' ); ?></option>
          <?php echo wp_get_archives($careers_archive); ?>
        </select>
      </form>
    </div>
  </div>
      <?php endif; ?>









</aside>


<!-- career blog sidebar -->


<?php

/**
 * Careers Custom Post Type
 */

function cpt_careers() {

  $label_plural   = 'Careers Blog';
  $label_singular = 'Career Post';

  $labels = array(
    'name'                  => _x( "{$label_plural}", 'Post Type General Name', 'boxpress' ),
    'singular_name'         => _x( "{$label_singular}", 'Post Type Singular Name', 'boxpress' ),
    'menu_name'             => __( "{$label_plural}", 'boxpress' ),
    'name_admin_bar'        => __( "{$label_singular}", 'boxpress' ),
    'parent_item_colon'     => __( "Parent {$label_singular}:", 'boxpress' ),
    'all_items'             => __( "All {$label_plural}", 'boxpress' ),
    'add_new_item'          => __( "Add New {$label_singular}", 'boxpress' ),
    'add_new'               => __( "Add New", 'boxpress' ),
    'new_item'              => __( "New {$label_singular}", 'boxpress' ),
    'edit_item'             => __( "Edit {$label_singular}", 'boxpress' ),
    'update_item'           => __( "Update {$label_singular}", 'boxpress' ),
    'view_item'             => __( "View {$label_singular}", 'boxpress' ),
    'search_items'          => __( "Search {$label_singular}", 'boxpress' ),
    'not_found'             => __( "Not found", 'boxpress' ),
    'not_found_in_trash'    => __( "Not found in Trash", 'boxpress' ),
    'items_list'            => __( "{$label_plural} list", 'boxpress' ),
    'items_list_navigation' => __( "{$label_plural} list navigation", 'boxpress' ),
    'filter_items_list'     => __( "Filter {$label_plural} list", 'boxpress' ),
  );
  $rewrite = array(
		'slug'                  => 'careers-blog-post',
		'with_front'            => true,
		'pages'                 => true,
		'feeds'                 => true,
	);
  $args = array(
    'label'                 => __( "{$label_plural}", 'boxpress' ),
    'description'           => __( "{$label_singular} Custom Post Type", 'boxpress' ),
    'labels'                => $labels,
    'supports'              => array( 'title', 'thumbnail', 'revisions', 'custom-fields', 'author', ),
    'hierarchical'          => false,
    'public'                => true,
    'show_ui'               => true,
    'show_in_menu'          => true,
    'menu_position'         => 5,
    'show_in_admin_bar'     => true,
    'show_in_nav_menus'     => true,
    'can_export'            => true,
    'has_archive'           => false,
    'exclude_from_search'   => false,
    'publicly_queryable'    => true,
    'rewrite'               => $rewrite,
    'capability_type'       => 'post',
  );

  register_post_type( 'careers', $args );
}
add_action( 'init', 'cpt_careers', 0 );


/**
 * Register Custom Taxonomy
 */
function taxonomy_career_categories() {

  $labels = array(
    'name'                       => _x( 'Categories', 'Taxonomy General Name', 'boxpress' ),
    'singular_name'              => _x( 'Category', 'Taxonomy Singular Name', 'boxpress' ),
    'menu_name'                  => __( 'Categories', 'boxpress' ),
    'all_items'                  => __( 'All Categories', 'boxpress' ),
    'parent_item'                => __( 'Parent Category', 'boxpress' ),
    'parent_item_colon'          => __( 'Parent Category:', 'boxpress' ),
    'new_item_name'              => __( 'New Category Name', 'boxpress' ),
    'add_new_item'               => __( 'Add New Category', 'boxpress' ),
    'edit_item'                  => __( 'Edit Category', 'boxpress' ),
    'update_item'                => __( 'Update Category', 'boxpress' ),
    'view_item'                  => __( 'View Category', 'boxpress' ),
    'separate_items_with_commas' => __( 'Separate categories with commas', 'boxpress' ),
    'add_or_remove_items'        => __( 'Add or remove categories', 'boxpress' ),
    'choose_from_most_used'      => __( 'Choose from the most used', 'boxpress' ),
    'popular_items'              => __( 'Popular Categories', 'boxpress' ),
    'search_items'               => __( 'Search Categories', 'boxpress' ),
    'not_found'                  => __( 'Not Found', 'boxpress' ),
    'items_list'                 => __( 'Categories list', 'boxpress' ),
    'items_list_navigation'      => __( 'Categories list navigation', 'boxpress' ),
  );
  $args = array(
    'labels'                     => $labels,
    'hierarchical'               => true,
    'public'                     => true,
    'show_ui'                    => true,
    'show_admin_column'          => true,
    'show_in_nav_menus'          => true,
    'show_tagcloud'              => false,
  );
  register_taxonomy( 'careers_categories', array( 'careers' ), $args );

}
add_action( 'init', 'taxonomy_career_categories', 0 );



// custom post type codepress_get_lang(



/**
 * CAREERS bloginfo
 */

require get_template_directory() . '/inc/cpt/careers-blog.php';

//  function
