<?php
/**
 * The template for displaying 404 pages (not found).
 *
 * @package BoxPress
 */

get_header(); ?>

  <?php require_once('template-parts/banners/banner--404.php'); ?>

  <section class="section">
    <div class="wrap wrap--limited">

      <article class="404-article">
        <header class="page-header">
          <h1 class="page-title">
            <?php _e( 'Sorry, this page can&rsquo;t be found.', 'boxpress' ); ?>
          </h1>
        </header>

        <div class="page-content">

          <p>
            <?php _e( 'It looks like nothing was found at this location. Maybe try one of the links below or a search?', 'boxpress' ); ?>
          </p>

          <?php get_template_part( 'template-parts/search-bar' ); ?>

          <?php the_widget( 'WP_Widget_Recent_Posts' ); ?>

          <?php if ( boxpress_categorized_blog() ) : ?>

            <div class="widget widget_categories">
              <h2 class="widget-title"><?php _e( 'Most Used Categories', 'boxpress' ); ?></h2>
              <ul>
                <?php
                  wp_list_categories( array(
                    'orderby'    => 'count',
                    'order'      => 'DESC',
                    'show_count' => 1,
                    'title_li'   => '',
                    'number'     => 10,
                  ));
                ?>
              </ul>
            </div>

          <?php endif; ?>

        </div>
      </article>

    </div>
  </section>

<?php get_footer(); ?>
