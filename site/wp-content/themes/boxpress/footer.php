
</main>
<footer id="colophon" class="site-footer" role="contentinfo">
  <div class="primary-footer">
    <div class="wrap">

      <div class="footer-col-1">
        <div class="site-branding">
          <a href="<?php echo esc_url( home_url( '/' )); ?>" rel="home">
            <img src="<?php echo get_bloginfo( 'template_directory' ); ?>/assets/img/branding/site-logo.png" />
          </a>
          <address>
            <?php get_template_part( 'template-parts/address-block' ); ?>
          </address>
        </div>
      </div>
      <div class="footer-col-2">
        <div class="site-copyright">
          <p><small><?php _e('Copyright', 'boxpress'); ?> &copy; <?php echo date('Y'); ?> <?php bloginfo( 'name' ); ?>. <?php _e('All rights reserved.', 'boxpress'); ?></small></p>
        </div>
        <div class="imagebox">
            <div class="footer-site-branding">
              <p><small><?php _e('Website by', 'boxpress'); ?></small></p>
              <a href="<?php echo esc_url( home_url( '/' )); ?>" rel="home">
                <img src="<?php echo get_bloginfo( 'template_directory' ); ?>/assets/img/branding/imagebox.png" />
              </a>
            </div>
        </div>
      </div>

    </div>
  </div>
  <div class="site-info">
    <div class="wrap">

    </div>
  </div>
</footer>

<?php wp_footer(); ?>

<?php // GA Tracking Code ?>
<?php the_field( 'tracking_code', 'option' ); ?>

</body>
</html>
