<?php

/**
 * Editor Formats
 */

function boxpress_mce_buttons( $buttons ) {
  array_unshift( $buttons, 'styleselect' );
  return $buttons;
}
add_filter( 'mce_buttons_2', 'boxpress_mce_buttons' );

function boxpress_mce_before_init_insert_formats( $init_array ) {
  $style_formats = array(
    array(
      'title'     => 'Button Orange',
      'selector'  => 'a',
      'classes'   => 'button button--icon-3',
    ),
    array(
      'title'     => 'Button Blue',
      'selector'  => 'a',
      'classes'   => 'button button--icon-4',
    ),
      array(
      'title'     => 'Lead Paragraph',
      'selector'  => 'p',
      'classes'   => 'p-lead',
  ),
  );
  $init_array['style_formats'] = json_encode( $style_formats );

  return $init_array;
}
add_filter( 'tiny_mce_before_init', 'boxpress_mce_before_init_insert_formats' );


/**
 * Editor Styles
 */

function boxpress_theme_add_editor_styles() {
  add_editor_style( 'custom-editor-style.css' );
}
add_action( 'admin_init', 'boxpress_theme_add_editor_styles' );
