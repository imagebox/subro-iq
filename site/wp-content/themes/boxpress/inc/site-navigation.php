<?php

/*
 * Navigation
 */

register_nav_menus( array(
  'primary'    => __( 'Primary Menu', 'boxpress' ),
  'secondary'  => __( 'Secondary Menu', 'boxpress' ),
  // 'button'     => __( 'Button Menu', 'boxpress' ),
));
