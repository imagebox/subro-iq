<?php
/**
 * Department Custom Post Type
 */

function cpt_location() {

  $label_plural   = 'Locations';
  $label_singular = 'Location';

  $labels = array(
    'name'                  => _x( "{$label_plural}", 'Post Type General Name', 'boxpress' ),
    'singular_name'         => _x( "{$label_singular}", 'Post Type Singular Name', 'boxpress' ),
    'menu_name'             => __( "{$label_plural}", 'boxpress' ),
    'name_admin_bar'        => __( "{$label_singular}", 'boxpress' ),
    'parent_item_colon'     => __( "Parent {$label_singular}:", 'boxpress' ),
    'all_items'             => __( "All {$label_plural}", 'boxpress' ),
    'add_new_item'          => __( "Add New {$label_singular}", 'boxpress' ),
    'add_new'               => __( "Add New", 'boxpress' ),
    'new_item'              => __( "New {$label_singular}", 'boxpress' ),
    'edit_item'             => __( "Edit {$label_singular}", 'boxpress' ),
    'update_item'           => __( "Update {$label_singular}", 'boxpress' ),
    'view_item'             => __( "View {$label_singular}", 'boxpress' ),
    'search_items'          => __( "Search {$label_singular}", 'boxpress' ),
    'not_found'             => __( "Not found", 'boxpress' ),
    'not_found_in_trash'    => __( "Not found in Trash", 'boxpress' ),
    'items_list'            => __( "{$label_plural} list", 'boxpress' ),
    'items_list_navigation' => __( "{$label_plural} list navigation", 'boxpress' ),
    'filter_items_list'     => __( "Filter {$label_plural} list", 'boxpress' ),
  );

  $args = array(
    'label'                 => __( "{$label_plural}", 'boxpress' ),
    'description'           => __( "{$label_singular} Custom Post Type", 'boxpress' ),
    'labels'                => $labels,
    'supports'              => array( 'title', 'editor', 'thumbnail', 'revisions', 'custom-fields' ),
    'hierarchical'          => false,
    'public'                => true,
    'show_ui'               => true,
    'show_in_menu'          => true,
    'menu_position'         => 5,
    'show_in_admin_bar'     => true,
    'show_in_nav_menus'     => true,
    'can_export'            => true,
    'has_archive'           => true,
    'exclude_from_search'   => false,
    'publicly_queryable'    => true,
    'capability_type'       => 'post',
    'rewrite' => array(
      'with_front' => false,
      'slug' => 'location',
    ),
  );

  register_post_type( 'location', $args );
}
add_action( 'init', 'cpt_location', 0 );


/**
 * Create Location Specialty Taxonomy
 */

function tax_location_type() {

  $label_plural   = 'Location Types';
  $label_singular = 'Location Type';

  $labels = array(
    'name'                       => _x( "{$label_plural}", 'Taxonomy General Name', 'boxpress' ),
    'singular_name'              => _x( "{$label_singular}", 'Taxonomy Singular Name', 'boxpress' ),
    'menu_name'                  => __( "{$label_plural}", 'boxpress' ),
    'all_items'                  => __( "All {$label_plural}", 'boxpress' ),
    'parent_item'                => __( "Parent {$label_singular}", 'boxpress' ),
    'parent_item_colon'          => __( "Parent {$label_singular}:", 'boxpress' ),
    'new_item_name'              => __( "New {$label_singular} Name", 'boxpress' ),
    'add_new_item'               => __( "Add New {$label_singular}", 'boxpress' ),
    'edit_item'                  => __( "Edit {$label_singular}", 'boxpress' ),
    'update_item'                => __( "Update {$label_singular}", 'boxpress' ),
    'view_item'                  => __( "View {$label_singular}", 'boxpress' ),
    'separate_items_with_commas' => __( "Separate {$label_plural} with commas", 'boxpress' ),
    'add_or_remove_items'        => __( "Add or remove {$label_plural}", 'boxpress' ),
    'choose_from_most_used'      => __( "Choose from the most used", 'boxpress' ),
    'popular_items'              => __( "Popular {$label_plural}", 'boxpress' ),
    'search_items'               => __( "Search {$label_plural}", 'boxpress' ),
    'not_found'                  => __( "Not Found", 'boxpress' ),
    'items_list'                 => __( "{$label_plural} list", 'boxpress' ),
    'items_list_navigation'      => __( "{$label_plural} list navigation", 'boxpress' ),
  );
  $args = array(
    'labels'                     => $labels,
    'hierarchical'               => true,
    'public'                     => true,
    'show_ui'                    => true,
    'show_admin_column'          => true,
    'show_in_nav_menus'          => true,
    'show_tagcloud'              => false,
    'rewrite' => array(
      'with_front' => false,
      'slug' => 'location-type',
    ),
  );

  // For location sorting
  register_taxonomy( 'location_type', 'location', $args );
}
add_action( 'init', 'tax_location_type' );
