<?php

/**
 * Clean Head
 * ---
 */

function boxpress_headspace() {
  remove_action('wp_head', 'rsd_link');
  remove_action('wp_head', 'wp_generator');

  remove_action('wp_head', 'feed_links', 2);
  remove_action('wp_head', 'feed_links_extra', 3);

  remove_action('wp_head', 'index_rel_link');
  remove_action('wp_head', 'wlwmanifest_link');

  remove_action('wp_head', 'start_post_rel_link', 10, 0);
  remove_action('wp_head', 'parent_post_rel_link', 10, 0);
  remove_action('wp_head', 'adjacent_posts_rel_link', 10, 0);
  remove_action('wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0 );

  remove_action('wp_head', 'wp_shortlink_wp_head', 10, 0 );
}
add_action('after_setup_theme', 'boxpress_headspace');



/**
 * Clean Admin Bar
 * ---
 */

add_action( 'admin_bar_menu', 'remove_admin_bar_items', 999 );
function remove_admin_bar_items( $wp_admin_bar ) {
  // Remove admin bar buttons
  $wp_admin_bar->remove_node( 'customize' );
  $wp_admin_bar->remove_node( 'comments' );
}



/**
 * Disable Emojis
 * ---
 */

// Remove actions & filters
function disable_wp_emojicons() {
  remove_action( 'admin_print_styles', 'print_emoji_styles' );
  remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
  remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
  remove_action( 'wp_print_styles', 'print_emoji_styles' );
  remove_filter( 'wp_mail', 'wp_staticize_emoji_for_email' );
  remove_filter( 'the_content_feed', 'wp_staticize_emoji' );
  remove_filter( 'comment_text_rss', 'wp_staticize_emoji' );
  add_filter( 'tiny_mce_plugins', 'disable_emojicons_tinymce' );  
}
add_action( 'init', 'disable_wp_emojicons' );

// Remove tinymce functionality
function disable_emojicons_tinymce( $plugins ) {
  if ( is_array( $plugins ) ) {
    return array_diff( $plugins, array( 'wpemoji' ) );
  } else {
    return array();
  }
}

// Remove DNS Prefetch
add_filter( 'emoji_svg_url', '__return_false' );


/**
 * Remove Comments
 */

// Remove comments from pages
add_action( 'init', 'remove_comment_support', 100 );
function remove_comment_support() {
  remove_post_type_support( 'page', 'comments' );
  // remove_post_type_support( 'post', 'comments' );
}


/**
 * CPT Nav Nav Fix
 * ---
 * Remove active "blog" link class from menu on Resources custom post type
 */

function custom_fix_blog_tab_on_cpt( $classes, $item, $args ) {
  if ( ! is_singular('post') &&
       ! is_category() &&
       ! is_tag() ) {

    $blog_page_id = intval( get_option( 'page_for_posts' ));

    if ( $item->object_id == $blog_page_id ) {
      unset( $classes[ array_search( 'current_page_parent', $classes )]);
    }
  }
  return $classes;
}

add_filter('nav_menu_css_class', 'custom_fix_blog_tab_on_cpt', 10, 3);



/**
 * Clean Excerpt
 * ---
 * Remove shortcodes and headings from excerpt content
 */

function boxpress_custom_excerpt( $excerpt = '' ) {

  $raw_excerpt = $excerpt;

  // Set the excerpt if empty
  if ( '' == $excerpt ) {
    $excerpt = get_the_content('');

    // Set Advanced Template Page Excerpt
    if ( have_rows('innerpage_master') ) {
      while ( have_rows('innerpage_master') ) { the_row();
        if ( get_row_layout() == 'full-width_column' ) {
          $excerpt = get_sub_field('content');
        }
      }
    }
  }

  $excerpt = strip_shortcodes( $excerpt );
  $excerpt = apply_filters('the_content', $excerpt);
  $excerpt = str_replace(']]>', ']]&gt;', $excerpt);

  $regex    = '#(<h([1-6])[^>]*>)\s?(.*)?\s?(<\/h\2>)#';
  $excerpt  = preg_replace( $regex,'', $excerpt );

  $excerpt_length = apply_filters( 'excerpt_length', 30 );
  $excerpt_more   = apply_filters( 'excerpt_more', ' ' . '&hellip;' );
  $excerpt        = wp_trim_words( $excerpt, $excerpt_length, $excerpt_more );

  return apply_filters( 'wp_trim_excerpt', preg_replace( $regex, '', $excerpt ), $raw_excerpt );
}
add_filter( 'get_the_excerpt', 'boxpress_custom_excerpt', 9);



/**
 * Clean Shortcodes
 * ---
 * Remove `p` and `br` tags from any shortcodes in content
 */

function boxpress_clean_shortcodes( $content ) {   
  
  // els to remove
  $array = array(
    '<p>['    => '[',
    ']</p>'   => ']',
    '<div>['  => '[',
    ']</div>' => ']',
    ']<br />' => ']',
    ']<br>'   => ']',
    '<br />[' => '[',
    '<br>['   => '[',
  );

  // Remove dem els
  $content = strtr( $content, $array );
  return $content;
}
add_filter('the_content', 'boxpress_clean_shortcodes');
