<!DOCTYPE html>
<html class="no-js" <?php language_attributes(); ?>>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="profile" href="http://gmpg.org/xfn/11">

  <?php wp_head(); ?>

</head>
<body <?php body_class(); ?>>

<?php // SVGs ?>
<?php include_once('template-parts/svg.php'); ?>

<?php // Mobile Header ?>
<?php include_once('template-parts/header-mobile.php'); ?>

<header id="masthead" class="site-header" role="banner">
  <div class="wrap">

    <div class="site-header-inner">
      <div class="header-col-1">

        <div class="site-branding">
          <a href="<?php echo esc_url( home_url( '/' )); ?>" rel="home">
          <img src="<?php bloginfo('template_directory'); ?>/assets/img/branding/paragon-logo.png" />
          </a>
        </div>

      </div>
      <div class="header-col-2 push-down">

        <?php if ( has_nav_menu( 'primary' )) : ?>

          <nav class="js-accessible-menu navigation--main"
            aria-label="<?php _e( 'Main Navigation', 'boxpress' ); ?>"
            role="navigation">
            <div class="top-menu-bar-phone">
              <?php the_field('top_bar_phone'); ?>
            </div>
            <ul class="nav-list">
                <?php
                  wp_nav_menu( array(
                    'theme_location'  => 'primary',
                    'items_wrap'      => '%3$s',
                    'container'       => false,
                    'walker'          => new Aria_Walker_Nav_Menu(),
                  ));
                ?>
            </ul>
          </nav>

        <?php endif; ?>

      </div>
    </div>
  </div>
</header>

<main id="main" class="site-main" role="main">
