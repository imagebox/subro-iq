
<section class="callout-block">
  <div class="wrap">
    <?php
     $callout_image_block_1 = get_field('callout_image_block_1');
     if( !empty($callout_image_block_1)):
       $url = $callout_image_block_2['url'];
       $size = 'intro_img';
       $my_callout_image_1 = $callout_image_block_1['sizes'][ $size ];
     ?>
     <div class="callout-image-block callout--right">
         <div class="callout-image text-align--right" style="background-image:url(<?php echo $my_callout_image_1; ?>); background-size: cover; background-repeat: no-repeat;">
         <div class="overlay-2"></div>
        <div class="callout-heading switch-margin--right">
             <h2><?php the_field('callout_heading_block_1') ?></h2>
        </div>
         </div>
     </div>

    <?php endif; ?>
    <!-- intro block  -->



     <div class="callout-content-block">
             <div class="callout-content pad--med">
               <?php $callout_content_1 = get_field('callout_content_1'); ?>
               <?php echo $callout_content_1; ?>

               <?php
                $callout_link_block_1 = get_field('callout_link_block_2');
                if( $callout_link_block_1): ?>
                 <a class="button button--icon-1" href="<?php echo $callout_link_block_1['url']; ?>" target="<?php echo $callout_link_block_1['target']; ?>"><?php echo $callout_link_block_1['title']; ?>  <svg width="20" height="8">
                     <use xlink:href="#arrow-button"></use>
                   </svg></a>
                <?php endif; ?>
             </div>
       </div>
     </div>
    </div>
    <!-- carousel-block  -->
  </div>
</section>
<!-- callout block 1  -->
