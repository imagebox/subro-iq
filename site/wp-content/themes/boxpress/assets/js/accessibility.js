(function ($) {
  'use strict';


  /**
   * Aria Nav Controls
   */

  var $aria_nav = $('.js-accessible-menu');
  $aria_nav.wpAriaNav();



  /**
   * Force all article external links to open in new tabs
   * ---
   * Applies only to anchor tags that don't already
   * have a target attribute
   */

  // Ignore article links that already have a target set,
  // link to an anchor, and link to a relative URL
  var $article_links = $('.entry-content a:not([target]):not([href^="#"]):not([href^="/"])');

  // Get the site's base URL
  var pathArray = location.href.split( '/' );
  var protocol  = pathArray[0];
  var host      = pathArray[2]; // ex. 'www.imagebox.com', 'domain.com'

  $article_links.each(function () {
    var this_href = $(this).attr('href');
    
    if ( typeof this_href !== typeof undefined && this_href !== false ) {
      // Check if link is external
      if ( this_href.indexOf( host ) === -1 ) {
        $(this).attr( 'target', '_blank' );
      }
    }
  });

})(jQuery);
