(function ($) {
  'use strict';

  /**
   * Site javascripts
   */


     // Video(s)
       $('.video-button').magnificPopup({
         type: 'iframe'
       });


  // Wrap iframes in div w/ flexible-container class to make responsive
  $("iframe:not(.not-responsive)").wrap('<div class="flexible-container"></div>');


  var $owl_carousel = $('.js-owl-carousel');

  $owl_carousel.each(function () {
    var $this = $(this);
    var carousel_loop = false;
    var carousel_nav  = false;
    var carousel_dots = false;
    var mouse_drag    = false;
    var touch_drag    = false;

    if ( $this.find( '> div' ).length > 1 ) {
      carousel_loop = true;
      carousel_nav  = true;
      carousel_dots = true;
      mouse_drag    = true;
      touch_drag    = true;
    }

    // Carousels
    $this.owlCarousel({
      items: 1,
      mouseDrag: mouse_drag,
      touchDrag: touch_drag,
      dots: carousel_dots,
      nav: carousel_nav,
      loop: carousel_loop,
      navSpeed: 500,
      dotsSpeed: 500,
      dragEndSpeed: 500,
      autoplayHoverPause: true,
      navText: [
        '<svg class="svg-left-icon" width="14" height="6"><use xlink:href="#carousel-arrow-left"></use></svg>',
        '<svg class="svg-right-icon" width="14" height="6"><use xlink:href="#carousel-arrow-right"></use></svg>',
      ]
    });
  });



})(jQuery);


// end accordions //
var accordions = document.getElementsByClassName('accordion');


for (var i = 0; i < accordions.length; i++){
  accordions[i].onclick = function () {
    this.classList.toggle('accord-is-open');
    var content = this.nextElementSibling;

    if (content.style.maxHeight) {
        content.style.maxHeight = null;
    } else {
      content.style.maxHeight = content.scrollHeight + "px";
    }
  };
}




// end accordions //
