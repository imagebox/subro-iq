(function ($) {
  'use strict';

  var $site_header = $('#masthead');
  $site_header.addClass('is-sticky');

  toggleStickyNav();

  $(window).on( 'scroll', function () {
    toggleStickyNav();
  });

  $(window).on( 'resize', function () {
    toggleStickyNav();
  });


  function toggleStickyNav() {
    var $site_header = $('#masthead');

    if ( $site_header.is( ':visible' ) &&
         $(window).scrollTop() > 40 ) {
      $site_header.addClass('minify-header');
    } else {
      $site_header.removeClass('minify-header');
    }
  }


  /**
   * Remove sticky header if user is scrolling via spacebar
   */

  // $('body').on('keydown', function () {
  //   // Spacebar
  //   if ( e.keyCode == 32 ) {
  //     $site_header.removeClass('minify-header is-sticky');
  //   }
  // });

})(jQuery);
