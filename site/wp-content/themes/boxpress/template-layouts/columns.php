<?php
/**
 * Displays the Column layout
 *
 * @package BoxPress
 */

$section_heading  = get_sub_field( 'section_heading' );
$background       = get_sub_field('background');

?>
<section class="fullwidth-column section <?php echo $background; ?>">
  <div class="wrap">

    <?php if ( ! empty( $section_heading )) : ?>
      <header class="section-header">
        <h3><?php echo $section_heading; ?></h3>
      </header>
    <?php endif; ?>

    <?php if ( have_rows( 'column_row' ) ) : ?>
      <?php while ( have_rows( 'column_row' ) ) : the_row();
          $total_cols = get_sub_field( 'number_of_columns' );
        ?>

        <div class="l-columns l-columns--<?php echo $total_cols; ?>">

          <?php for ( $i = 1; $i <= $total_cols; $i++ ) :
              $column_content = get_sub_field( 'column_' . $i );
            ?>

            <div class="l-column-item">
              <div class="page-content">

                <?php echo $column_content; ?>

              </div>
            </div>

          <?php endfor; ?>

        </div>

      <?php endwhile; ?>
    <?php endif; ?>

  </div>
</section>
