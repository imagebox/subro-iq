<?php
/**
 * Displays the Parallax Background layout
 *
 * @package BoxPress
 */

$quote_copy       = get_sub_field( 'quote_copy' );
$quote_citation   = get_sub_field( 'quote_citation' );
$quote_job_title  = get_sub_field( 'quote_job_title' );
$background       = get_sub_field( 'quote_background' );
$background_image = get_sub_field( 'quote_background_image' );

?>
<section class="section quote-block <?php echo $background; ?>"
  <?php
    if ( $background_image && $background === 'background-image' ) {
      echo "style=\"background-image: url('{$background_image['url']}')\"";
    }
  ?>>
  <div class="wrap">

    <blockquote>
      <div class="quote-icon">
        <svg class="quote-icon-svg" width="54" height="47">
          <use xlink:href="#quote-icon"></use>
        </svg>
      </div>
      <div class="quote-block-body">
        <?php echo $quote_copy; ?>
      </div>

      <?php if ( ! empty( $quote_citation ) ) : ?>

        <cite class="quote-block-citation">
          <span class="quote-name"><?php echo $quote_citation; ?></span>
          <?php if ( ! empty( $quote_job_title )) : ?>
            <span class="quote-title"><?php echo $quote_job_title; ?></span>
          <?php endif; ?>
        </cite>

      <?php endif; ?>

    </blockquote>

  </div>
</section>
