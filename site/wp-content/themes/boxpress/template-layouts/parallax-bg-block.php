<?php
/**
 * Displays the Parallax Background layout
 *
 * @package BoxPress
 */

$parallax_background_image = get_sub_field('parallax_background_image');
$parallax_content = get_sub_field('parallax_content');

?>
<?php if ( ! empty( $parallax_background_image )) : ?>
  <section class="section parallax"
    style="background-image: url(<?php echo esc_url( $parallax_background_image ); ?>); background-size: cover;">
    <div class="parallax-overlay"></div>
    <div class="wrap">
      <div class="parallax-content">
        <?php echo $parallax_content ?>
        </div>
    </div>
  </section>
<?php endif; ?>
