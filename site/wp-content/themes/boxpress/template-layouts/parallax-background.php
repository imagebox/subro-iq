<?php
/**
 * Displays the Parallax Background layout
 *
 * @package BoxPress
 */

$background_image = get_sub_field('image');

?>
<?php if ( ! empty( $background_image )) : ?>
  <section class="section parallax"
    style="background-image: url(<?php echo esc_url( $background_image ); ?>);">
  </section>
<?php endif; ?>
