<?php
/**
 * Displays the Button Block layout
 *
 * @package BoxPress
 */

$callout_title    = get_sub_field( 'button_block_title' );
$callout_copy     = get_sub_field( 'button_block_copy' );
$button_link      = get_sub_field( 'button_block_button_link' );
$background       = get_sub_field( 'button_block_background' );
$background_image = get_sub_field( 'button_block_background_image' );

?>
<section class="fullwidth-column section <?php echo $background; ?>"
  <?php if ( $background == 'background-image' && $background_image ) : ?>
    style="background-image: url('<?php echo $background_image['url']; ?>');"
  <?php endif; ?>>

  <div class="wrap wrap--limited">
    <div class="button-block">

      <?php if ( ! empty( $callout_title )) : ?>

        <div class="callout-header">
          <h2><?php echo $callout_title; ?></h2>
        </div>

      <?php endif; ?>

      <div class="callout-body">
        <div class="button-callout-content">
          <?php if ( ! empty( $callout_copy )) : ?>
            <p><?php echo $callout_copy; ?></p>
          <?php endif; ?>

          <?php if ( $button_link ) : ?>
            <a class="button"
              href="<?php echo esc_url( $button_link['url'] ); ?>"
              target="<?php echo $button_link['target']; ?>">
              <?php echo $button_link['title']; ?>
              <svg class="" width="48" height="33">
                <use xlink:href="#arrow-button"></use>
              </svg>
            </a>
          <?php endif; ?>

        </div>
      </div>
    </div>
  </div>
</section>
