<!DOCTYPE html>
<html class="no-js" <?php language_attributes(); ?>>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="profile" href="http://gmpg.org/xfn/11">

  <?php wp_head(); ?>

</head>
<body <?php body_class(); ?>>

<?php

$top_bar_phone  = get_field('top_bar_phone');

 ?>

<?php // SVGs ?>
<?php include_once('template-parts/svg.php'); ?>

<?php // Mobile Header ?>
<?php include_once('template-parts/header-mobile.php'); ?>

<header id="masthead" class="site-header <?php if(!is_front_page()) { echo 'innerpage-header';}?>" role="banner">

<div class="secondary-menu-top-bar">
  <div class="wrap">
    <?php if ( has_nav_menu( 'secondary' )) : ?>

      <nav class="js-accessible-menu navigation--main navigation--secondary nav-sub-top"
        aria-label="<?php _e( 'Secondary Navigation', 'boxpress' ); ?>"
        role="navigation">
        <div class="top-menu-bar-phone">
        <h5>CALL US AT<span> 800.723.1864</span></h5>
        </div>
        <ul class="nav-list">
            <?php
              wp_nav_menu( array(
                'theme_location'  => 'secondary',
                'items_wrap'      => '%3$s',
                'container'       => false,
                'walker'          => new Aria_Walker_Nav_Menu(),
              ));
            ?>
        </ul>
      </nav>

    <?php endif; ?>
  </div>
</div>

  <div class="wrap">

    <div class="site-header-inner">
      <div class="header-col-1">

        <div class="site-branding">
          <a href="<?php echo esc_url( home_url( '/' )); ?>" rel="home">
            <img src="<?php echo get_bloginfo( 'template_directory' ); ?>/assets/img/branding/site-logo.png" />
          </a>
        </div>

      </div>
      <div class="header-col-2 push-down">

        <?php if ( has_nav_menu( 'primary' )) : ?>

          <nav class="js-accessible-menu navigation--main nav-sub-bottom"
            aria-label="<?php _e( 'Main Navigation', 'boxpress' ); ?>"
            role="navigation">
            <ul class="nav-list">
                <?php
                  wp_nav_menu( array(
                    'theme_location'  => 'primary',
                    'items_wrap'      => '%3$s',
                    'container'       => false,
                    'walker'          => new Aria_Walker_Nav_Menu(),
                  ));
                ?>
            </ul>
          </nav>

        <?php endif; ?>

      </div>
    </div>
  </div>


</header>

<main id="main" class="site-main" role="main">
