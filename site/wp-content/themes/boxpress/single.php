<?php
/**
 * The template for displaying all single posts.
 *
 * @package BoxPress
 */

get_header(); ?>

  <?php require_once('template-parts/banners/banner--blog.php'); ?>

  <section class="blog-page">
    <div class="wrap">
      <div class="l-sidebar">
        <div class="l-main">

          <?php while ( have_posts() ) : the_post(); ?>

            <?php get_template_part( 'template-parts/content', 'single' ); ?>

            <?php the_post_navigation(); ?>

            <?php
              // Load Comment Block
              if ( comments_open() || get_comments_number() ) :
                comments_template();
              endif;
            ?>

          <?php endwhile; ?>
          
        </div>
        <div class="l-aside">

          <?php get_sidebar(); ?>

        </div>
      </div>
    </div>
  </section>

<?php get_footer(); ?>